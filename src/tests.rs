use crate::{header::Colorspace, *};

#[test]
fn empty_image_read() {
    let img = qoi::deserialize(include_bytes!("../assets/empty.qoi")).unwrap();
    assert_eq!(img.ops, vec![]);
    let qoi::Image { header: _, pixels } = qoi::decode(&img).unwrap();
    assert_eq!(pixels, vec![]);
}

#[test]
fn rgb_strip_read() {
    let img = qoi::deserialize(include_bytes!("../assets/rgb_strip.qoi")).unwrap();
    assert_eq!(
        img.ops,
        vec![
            qoi::QOI_OP::RGB { r: 255, g: 0, b: 0 },
            qoi::QOI_OP::RUN { times: 14 },
            qoi::QOI_OP::RGB { r: 0, g: 255, b: 0 },
            qoi::QOI_OP::RUN { times: 14 },
            qoi::QOI_OP::RGB { r: 0, g: 0, b: 255 },
            qoi::QOI_OP::RUN { times: 14 },
        ]
    );
    let qoi::Image { header: _, pixels } = qoi::decode(&img).unwrap();
    let mut expected = vec![
        pixel::Pixel {
            r: 255,
            g: 0,
            b: 0,
            a: 255
        };
        15
    ];
    expected.append(&mut vec![
        pixel::Pixel {
            r: 0,
            g: 255,
            b: 0,
            a: 255
        };
        15
    ]);
    expected.append(&mut vec![
        pixel::Pixel {
            r: 0,
            g: 0,
            b: 255,
            a: 255
        };
        15
    ]);
    assert_eq!(pixels, expected);
}

#[test]
fn red_strip_read() {
    let img1 = qoi::deserialize(include_bytes!("../assets/red_strip.qoi")).unwrap();
    assert_eq!(img1.ops, vec![qoi::QOI_OP::RGB { r: 255, g: 0, b: 0 }; 15]);
    let img2 = qoi::deserialize(include_bytes!("../assets/red_strip_run.qoi")).unwrap();
    assert_eq!(
        img2.ops,
        vec![
            qoi::QOI_OP::RGB { r: 255, g: 0, b: 0 },
            qoi::QOI_OP::RUN { times: 14 },
        ]
    );
    let qoi::Image {
        header: _,
        pixels: pix1,
    } = qoi::decode(&img1).unwrap();
    let qoi::Image {
        header: _,
        pixels: pix2,
    } = qoi::decode(&img2).unwrap();
    assert_eq!(pix1, pix2);
    assert_eq!(
        pix1,
        vec![
            pixel::Pixel {
                r: 255,
                g: 0,
                b: 0,
                a: 255
            };
            15
        ]
    );
}

#[test]
fn empty_image_encode() {
    let image = qoi::encode(&qoi::Image {
        header: header::Header {
            magic: ['q', 'o', 'i', 'f'],
            width: 0,
            height: 0,
            channels: 4,
            colorspace: Colorspace::SRGB,
        },
        pixels: vec![],
    })
    .unwrap();

    assert_eq!(image.ops, vec![]);

    let serialized = qoi::serialize(&image).unwrap();

    assert_eq!(serialized, include_bytes!("../assets/empty.qoi"))
}

#[test]
fn red_strip_encode() {
    let image = qoi::encode(&qoi::Image {
        header: header::Header {
            magic: ['q', 'o', 'i', 'f'],
            width: 15,
            height: 1,
            channels: 4,
            colorspace: Colorspace::SRGB,
        },
        pixels: vec![
            pixel::Pixel {
                r: 255,
                g: 0,
                b: 0,
                a: 255
            };
            15
        ],
    })
    .unwrap();

    assert_eq!(
        image.ops,
        vec![
            qoi::QOI_OP::RGB { r: 255, g: 0, b: 0 },
            qoi::QOI_OP::RUN { times: 14 },
        ]
    );

    let serialized = qoi::serialize(&image).unwrap();

    assert_eq!(serialized, include_bytes!("../assets/red_strip_run.qoi"))
}

#[test]
fn rgb_strip_encode() {
    let mut pixels = vec![
        pixel::Pixel {
            r: 255,
            g: 0,
            b: 0,
            a: 255
        };
        15
    ];
    pixels.append(&mut vec![
        pixel::Pixel {
            r: 0,
            g: 255,
            b: 0,
            a: 255
        };
        15
    ]);
    pixels.append(&mut vec![
        pixel::Pixel {
            r: 0,
            g: 0,
            b: 255,
            a: 255
        };
        15
    ]);

    let image = qoi::encode(&qoi::Image {
        header: header::Header {
            magic: ['q', 'o', 'i', 'f'],
            width: 15,
            height: 3,
            channels: 4,
            colorspace: Colorspace::SRGB,
        },
        pixels,
    })
    .unwrap();

    assert_eq!(
        image.ops,
        vec![
            qoi::QOI_OP::RGB { r: 255, g: 0, b: 0 },
            qoi::QOI_OP::RUN { times: 14 },
            qoi::QOI_OP::RGB { r: 0, g: 255, b: 0 },
            qoi::QOI_OP::RUN { times: 14 },
            qoi::QOI_OP::RGB { r: 0, g: 0, b: 255 },
            qoi::QOI_OP::RUN { times: 14 },
        ]
    );

    let serialized = qoi::serialize(&image).unwrap();

    assert_eq!(serialized, include_bytes!("../assets/rgb_strip.qoi"))
}

#[test]
fn serialization() {
    assert_eq!(
        qoi::QOI_OP::RGB {
            r: 123,
            g: 100,
            b: 169
        }
        .serialize()
        .unwrap(),
        vec![0b11111110, 123, 100, 169]
    );

    assert_eq!(
        qoi::QOI_OP::RGBA {
            r: 123,
            g: 100,
            b: 169,
            a: 200,
        }
        .serialize()
        .unwrap(),
        vec![0b11111111, 123, 100, 169, 200]
    );

    for i in 0..100 {
        if i < 64 {
            assert_eq!(
                qoi::QOI_OP::INDEX { i }.serialize().unwrap(),
                vec![0b00111111 & (i as u8)]
            );
        } else {
            assert_eq!(
                qoi::QOI_OP::INDEX { i }.serialize().unwrap_err(),
                error::QoiError::InvalidIndex
            );
        }
    }

    for dr in -10..=10 {
        for dg in -10..=10 {
            for db in -10..=10 {
                if dr >= -2 && dr <= 1 && dg >= -2 && dg <= 1 && db >= -2 && db <= 1 {
                    assert_eq!(
                        qoi::QOI_OP::DIFF { dr, dg, db }.serialize().unwrap(),
                        vec![
                            0b01000000
                                | (((dr + 2) as u8) << 4)
                                | (((dg + 2) as u8) << 2)
                                | (((db + 2) as u8) << 0)
                        ],
                        "failed QOI_OP::DIFF with {}, {}, {}",
                        dr,
                        dg,
                        db
                    );
                } else {
                    assert_eq!(
                        qoi::QOI_OP::DIFF { dr, dg, db }.serialize().unwrap_err(),
                        error::QoiError::InvalidDiff
                    );
                }
            }
        }
    }

    for dg in -100..=100 {
        for dr_dg in -16..=16 {
            for db_dg in -16..=16 {
                if dg >= -32 && dg <= 31 && dr_dg >= -8 && dr_dg <= 7 && db_dg >= -8 && db_dg <= 7 {
                    assert_eq!(
                        qoi::QOI_OP::LUMA { dg, dr_dg, db_dg }.serialize().unwrap(),
                        vec![
                            0b10000000 | (((dg + 32) as u8) & 0b00111111),
                            (((dr_dg + 8) as u8) << 4) | (((db_dg + 8) as u8) << 0)
                        ],
                        "failed QOI_OP::LUMA with {}, {}, {}",
                        dg,
                        dr_dg,
                        db_dg
                    );
                } else {
                    assert_eq!(
                        qoi::QOI_OP::LUMA { dg, dr_dg, db_dg }
                            .serialize()
                            .unwrap_err(),
                        error::QoiError::InvalidLuma
                    );
                }
            }
        }
    }

    for i in 0..100 {
        if i >= 1 && i <= 62 {
            assert_eq!(
                qoi::QOI_OP::RUN { times: i }.serialize().unwrap(),
                vec![0b11000000 | ((i - 1) as u8)]
            );
        } else {
            assert_eq!(
                qoi::QOI_OP::RUN { times: i }.serialize().unwrap_err(),
                error::QoiError::InvalidRun
            );
        }
    }
}

#[test]
fn sum_with_wraparound() {
    assert_eq!(pixel::sum_with_wraparound(255, 0), 255);
    assert_eq!(pixel::sum_with_wraparound(255, 1), 0);
    assert_eq!(pixel::sum_with_wraparound(255, 20), 19);
    assert_eq!(pixel::sum_with_wraparound(0, -1), 255);
    assert_eq!(pixel::sum_with_wraparound(1, -2), 255);
    assert_eq!(pixel::sum_with_wraparound(0, -2), 255 - 1);
    assert_eq!(pixel::sum_with_wraparound(0, -30), 255 - 29);
}
