use std::ops::{Index, IndexMut};

use crate::header::*;
use crate::qoi::*;

pub(crate) struct EncodedImage {
    pub(crate) header: Header,
    pub(crate) ops: Vec<QOI_OP>,
}

pub struct Image {
    pub header: Header,
    pub pixels: Vec<Pixel>,
}

impl Image {
    pub fn solid_color(width: usize, height: usize, color: Pixel) -> Self {
        Self {
            header: Header::new(width, height, 4, Colorspace::SRGB),
            pixels: vec![color; width * height],
        }
    }

    pub fn blank(width: usize, height: usize) -> Self {
        Self::solid_color(width, height, Pixel::rgb(0, 0, 0))
    }

    pub fn get_width(&self) -> usize {
        self.header.width.try_into().unwrap_or(0)
    }

    pub fn get_height(&self) -> usize {
        self.header.height.try_into().unwrap_or(0)
    }
}

impl Index<(usize, usize)> for Image {
    type Output = Pixel;
    fn index<'a>(&'a self, index: (usize, usize)) -> &Self::Output {
        &self.pixels[index.0 % self.header.width as usize + index.1 * self.header.width as usize]
    }
}
impl IndexMut<(usize, usize)> for Image {
    fn index_mut<'a>(&'a mut self, index: (usize, usize)) -> &mut Self::Output {
        &mut self.pixels
            [index.0 % self.header.width as usize + index.1 * self.header.width as usize]
    }
}

impl Index<usize> for Image {
    type Output = Pixel;
    fn index<'a>(&'a self, index: usize) -> &Self::Output {
        &self.pixels[index]
    }
}
impl IndexMut<usize> for Image {
    fn index_mut<'a>(&'a mut self, index: usize) -> &mut Self::Output {
        &mut self.pixels[index]
    }
}
