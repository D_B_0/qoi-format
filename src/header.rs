use crate::error::*;

#[derive(Debug, Clone, PartialEq)]
pub enum Colorspace {
    Linear,
    SRGB,
    Unrecognized,
}

impl From<u8> for Colorspace {
    fn from(c: u8) -> Self {
        match c {
            0 => Colorspace::SRGB,
            1 => Colorspace::Linear,
            _ => Colorspace::Unrecognized,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Header {
    pub(crate) magic: [char; 4],
    pub width: u32,
    pub height: u32,
    pub channels: u8,
    pub colorspace: Colorspace,
}

impl Header {
    pub fn new(width: usize, height: usize, channels: u8, colorspace: Colorspace) -> Self {
        Self {
            magic: ['q', 'o', 'i', 'f'],
            width: width as u32,
            height: height as u32,
            channels,
            colorspace,
        }
    }

    pub(crate) fn validate(&self) -> Result<(), QoiError> {
        if self.magic != ['q', 'o', 'i', 'f'] {
            return Err(QoiError::InvalidMagicBytes);
        }
        if self.channels != 3 && self.channels != 4 {
            return Err(QoiError::InvalidChannels);
        }
        if self.colorspace == Colorspace::Unrecognized {
            return Err(QoiError::UnrecognizedColorspace);
        }
        Ok(())
    }

    pub(crate) fn serialize(&self) -> Result<Vec<u8>, QoiError> {
        let mut res = vec!['q' as u8, 'o' as u8, 'i' as u8, 'f' as u8];
        res.append(&mut self.width.to_be_bytes().into());
        res.append(&mut self.height.to_be_bytes().into());
        res.push(self.channels);
        res.push(match self.colorspace {
            Colorspace::SRGB => 0,
            Colorspace::Linear => 1,
            _ => 2,
        });
        Ok(res)
    }
}
