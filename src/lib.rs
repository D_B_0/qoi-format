pub mod error;
pub mod header;
pub mod image;
pub mod pixel;
#[cfg(test)]
mod tests;

pub mod qoi {
    pub use crate::error::*;
    pub use crate::header::*;
    pub use crate::image::*;
    pub use crate::pixel::*;
    use std::{fs, io};

    #[derive(Debug, Clone, Copy, PartialEq)]
    #[allow(non_camel_case_types)]
    pub(crate) enum QOI_OP {
        RGB { r: u8, g: u8, b: u8 },
        RGBA { r: u8, g: u8, b: u8, a: u8 },
        INDEX { i: usize },
        DIFF { dr: i8, dg: i8, db: i8 },
        LUMA { dg: i8, dr_dg: i8, db_dg: i8 },
        RUN { times: u8 },
    }

    impl QOI_OP {
        pub(crate) fn serialize(&self) -> Result<Vec<u8>, QoiError> {
            Ok(match &self {
                QOI_OP::RGB { r, g, b } => vec![0b11111110, *r, *g, *b],
                QOI_OP::RGBA { r, g, b, a } => vec![0b11111111, *r, *g, *b, *a],
                QOI_OP::INDEX { i } => {
                    if i > &63 {
                        return Err(QoiError::InvalidIndex);
                    }
                    vec![0b00111111 & (*i as u8)]
                }
                QOI_OP::DIFF { dr, dg, db } => {
                    if !(-2..=1).contains(dr) || !(-2..=1).contains(dg) || !(-2..=1).contains(db) {
                        return Err(QoiError::InvalidDiff);
                    }
                    let u_dr = (dr + 2) as u8;
                    let u_dg = (dg + 2) as u8;
                    let u_db = (db + 2) as u8;
                    vec![
                        0b01000000
                            | ((u_dr << 4) & 0b00110000)
                            | ((u_dg << 2) & 0b00001100)
                            | ((u_db << 0) & 0b00000011),
                    ]
                }
                QOI_OP::LUMA { dg, dr_dg, db_dg } => {
                    if !(-32..=31).contains(dg)
                        || !(-8..=7).contains(dr_dg)
                        || !(-8..=7).contains(db_dg)
                    {
                        return Err(QoiError::InvalidLuma);
                    }
                    let u_dg = (*dg + 32) as u8;
                    let u_dr_dg = (*dr_dg + 8) as u8;
                    let u_db_dg = (*db_dg + 8) as u8;
                    vec![
                        0b10000000 | (0b00111111 & u_dg),
                        ((u_dr_dg << 4) & 0b11110000) | (u_db_dg & 0b00001111),
                    ]
                }
                QOI_OP::RUN { times } => {
                    if !(1..=62).contains(times) {
                        return Err(QoiError::InvalidRun);
                    }
                    vec![0b11000000 | (0b00111111 & *times - 1)]
                }
            })
        }
    }

    fn get_header<'a, I>(contents: &mut I) -> Option<Header>
    where
        I: Iterator<Item = &'a u8>,
    {
        let magic: [char; 4] = [
            (*contents.next()?).into(),
            (*contents.next()?).into(),
            (*contents.next()?).into(),
            (*contents.next()?).into(),
        ];
        let width = u32::from_be_bytes([
            *contents.next()?,
            *contents.next()?,
            *contents.next()?,
            *contents.next()?,
        ]);
        let height = u32::from_be_bytes([
            *contents.next()?,
            *contents.next()?,
            *contents.next()?,
            *contents.next()?,
        ]);
        let channels = *contents.next()?;
        let colorspace = Colorspace::from(*contents.next()?);
        Some(Header {
            magic,
            width,
            height,
            channels,
            colorspace,
        })
    }

    fn get_rgb<'a, I>(contents: &mut I) -> Option<(u8, u8, u8)>
    where
        I: Iterator<Item = &'a u8>,
    {
        Some((*contents.next()?, *contents.next()?, *contents.next()?))
    }

    fn get_rgba<'a, I>(contents: &mut I) -> Option<(u8, u8, u8, u8)>
    where
        I: Iterator<Item = &'a u8>,
    {
        Some((
            *contents.next()?,
            *contents.next()?,
            *contents.next()?,
            *contents.next()?,
        ))
    }

    pub fn read_from_file(filepath: &str) -> Result<Image, QoiError> {
        let image = deserialize(&fs::read(filepath)?)?;
        let res = decode(&image)?;
        Ok(res)
    }

    pub fn write_to_file(image: &Image, filepath: &str) -> Result<(), QoiError> {
        let encoded = encode(image)?;
        let serialized = serialize(&encoded)?;
        fs::write(filepath, serialized)?;
        Ok(())
    }

    pub(crate) fn deserialize(contents: &[u8]) -> Result<EncodedImage, QoiError> {
        let mut contents_iter = contents.iter();
        let header = if let Some(h) = get_header(&mut contents_iter) {
            h
        } else {
            return Err(QoiError::IncompleteHeader);
        };
        header.validate()?;

        let mut ops: Vec<QOI_OP> = Vec::with_capacity((header.width * header.height) as usize);
        loop {
            let chunk = if let Some(&c) = contents_iter.next() {
                c
            } else {
                break;
            };
            ops.push(if chunk == 0b11111110 {
                let (r, g, b) = if let Some(p) = get_rgb(&mut contents_iter) {
                    p
                } else {
                    return Err(io::Error::from(io::ErrorKind::UnexpectedEof).into());
                };
                QOI_OP::RGB { r, g, b }
            } else if chunk == 0b11111111 {
                let (r, g, b, a) = if let Some(p) = get_rgba(&mut contents_iter) {
                    p
                } else {
                    return Err(io::Error::from(io::ErrorKind::UnexpectedEof).into());
                };
                QOI_OP::RGBA { r, g, b, a }
            } else if chunk >> 6 == 0b00 {
                let i = chunk & 0b00111111;
                QOI_OP::INDEX { i: i as usize }
            } else if chunk >> 6 == 0b01 {
                let dr = ((chunk & 0b00110000) >> 4) as i8 - 2;
                let dg = ((chunk & 0b00001100) >> 2) as i8 - 2;
                let db = ((chunk & 0b00000011) >> 0) as i8 - 2;
                QOI_OP::DIFF { dr, dg, db }
            } else if chunk >> 6 == 0b10 {
                let dg = (chunk & 0b00111111) as i8 - 32;

                let next_chunk = if let Some(c) = contents_iter.next() {
                    *c
                } else {
                    return Err(io::Error::from(io::ErrorKind::UnexpectedEof).into());
                };
                let dr_dg = ((next_chunk & 0b11110000) >> 4) as i8 - 8;
                let db_dg = ((next_chunk & 0b00001111) >> 0) as i8 - 8;

                QOI_OP::LUMA { dg, dr_dg, db_dg }
            } else if chunk >> 6 == 0b11 {
                let times = (chunk & 0b00111111) + 1;
                QOI_OP::RUN { times }
            } else {
                unreachable!();
            });
        }
        Ok(EncodedImage { header, ops })
    }

    pub(crate) fn decode(image: &EncodedImage) -> Result<Image, QoiError> {
        let mut pixels: Vec<Pixel> =
            Vec::with_capacity((image.header.width * image.header.height) as usize);
        let mut pixel_arr = [Pixel {
            r: 0,
            g: 0,
            b: 0,
            a: 0,
        }; 64];
        let mut prev_pixel = Pixel {
            r: 0,
            g: 0,
            b: 0,
            a: 255,
        };
        for &op in &image.ops {
            match op {
                QOI_OP::RGB { r, g, b } => {
                    let p = Pixel {
                        r,
                        g,
                        b,
                        a: prev_pixel.a,
                    };
                    prev_pixel = p;
                    pixel_arr[p.hash()] = p;
                    pixels.push(p);
                }
                QOI_OP::RGBA { r, g, b, a } => {
                    let p = Pixel { r, g, b, a };
                    prev_pixel = p;
                    pixel_arr[p.hash()] = p;
                    pixels.push(p);
                }
                QOI_OP::INDEX { i } => {
                    let p = pixel_arr[i];
                    prev_pixel = p;
                    pixel_arr[p.hash()] = p;
                    pixels.push(p);
                }
                QOI_OP::DIFF { dr, dg, db } => {
                    let mut p = prev_pixel.clone();
                    p.add(dr, dg, db);
                    prev_pixel = p;
                    pixel_arr[p.hash()] = p;
                    pixels.push(p);
                }
                QOI_OP::LUMA { dg, dr_dg, db_dg } => {
                    let dr = dr_dg + dg;
                    let db = db_dg + dg;
                    let mut p = prev_pixel.clone();
                    p.add(dr, dg, db);
                    prev_pixel = p;
                    pixel_arr[p.hash()] = p;
                    pixels.push(p);
                }
                QOI_OP::RUN { times } => {
                    for _ in 0..times {
                        pixels.push(prev_pixel);
                    }
                }
            };

            if pixels.len() == (image.header.width * image.header.height) as usize {
                break;
            }
        }
        if pixels.len() != (image.header.width * image.header.height) as usize {
            return Err(QoiError::IncongruentWidthXHeightPixelCount);
        }
        Ok(Image {
            header: image.header.clone(),
            pixels,
        })
    }

    pub(crate) fn encode(image: &Image) -> Result<EncodedImage, QoiError> {
        let Image { header, pixels } = image;
        let mut image = EncodedImage {
            header: header.clone(),
            ops: vec![],
        };
        let mut pixel_arr = [Pixel {
            r: 0,
            g: 0,
            b: 0,
            a: 0,
        }; 64];
        let mut prev_pixel = Pixel {
            r: 0,
            g: 0,
            b: 0,
            a: 255,
        };
        for &pix in pixels {
            if let Some(&op) = image.ops.last() {
                match op {
                    QOI_OP::RUN { times } => {
                        if prev_pixel == pix {
                            if times < 62 {
                                image.ops.pop();
                                image.ops.push(QOI_OP::RUN { times: times + 1 });
                            } else {
                                image.ops.push(QOI_OP::RUN { times: 1 });
                            }
                            continue;
                        }
                    }
                    _ => {}
                }
            }

            if pix == prev_pixel {
                image.ops.push(QOI_OP::RUN { times: 1 });
                continue;
            }

            if let Some(i) = pixel_arr.iter().position(|&p| p == pix) {
                pixel_arr[pix.hash()] = pix;
                prev_pixel = pix;
                image.ops.push(QOI_OP::INDEX { i });
                continue;
            }
            let dr = pix.r as i16 - prev_pixel.r as i16;
            let dg = pix.g as i16 - prev_pixel.g as i16;
            let db = pix.b as i16 - prev_pixel.b as i16;

            if pix.a == prev_pixel.a
                && (-2..=1).contains(&dr)
                && (-2..=1).contains(&dg)
                && (-2..=1).contains(&db)
            {
                pixel_arr[pix.hash()] = pix;
                prev_pixel = pix;
                image.ops.push(QOI_OP::DIFF {
                    dr: dr as i8,
                    dg: dg as i8,
                    db: db as i8,
                });
                continue;
            }

            if (-32..=31).contains(&dg)
                && (-8..=7).contains(&(dr - dg))
                && (-8..=7).contains(&(db - dg))
            {
                pixel_arr[pix.hash()] = pix;
                prev_pixel = pix;
                image.ops.push(QOI_OP::LUMA {
                    dr_dg: (dr - dg) as i8,
                    dg: dg as i8,
                    db_dg: (db - dg) as i8,
                });
                continue;
            }

            if pix.a == prev_pixel.a {
                pixel_arr[pix.hash()] = pix;
                prev_pixel = pix;
                image.ops.push(QOI_OP::RGB {
                    r: pix.r,
                    g: pix.g,
                    b: pix.b,
                });
                continue;
            }

            pixel_arr[pix.hash()] = pix;
            prev_pixel = pix;
            image.ops.push(QOI_OP::RGBA {
                r: pix.r,
                g: pix.g,
                b: pix.b,
                a: pix.a,
            });
        }

        Ok(image)
    }

    pub(crate) fn serialize(image: &EncodedImage) -> Result<Vec<u8>, QoiError> {
        let mut res = image.header.serialize()?;
        for op in &image.ops {
            res.append(&mut op.serialize()?);
        }
        Ok(res)
    }
}
