use std::io;

#[derive(Debug)]
pub enum QoiError {
    InvalidMagicBytes,
    InvalidChannels,
    UnrecognizedColorspace,
    IncompleteHeader,
    IncongruentWidthXHeightPixelCount,
    InvalidIndex,
    InvalidDiff,
    InvalidLuma,
    InvalidRun,
    IoError(io::Error),
}

impl PartialEq for QoiError {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::IoError(_), Self::IoError(_)) => true,
            _ => core::mem::discriminant(self) == core::mem::discriminant(other),
        }
    }
}

impl From<io::Error> for QoiError {
    fn from(e: io::Error) -> Self {
        Self::IoError(e)
    }
}
