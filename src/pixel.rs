#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Pixel {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Pixel {
    pub(crate) fn hash(&self) -> usize {
        (self.r as usize * 3 + self.g as usize * 5 + self.b as usize * 7 + self.a as usize * 11)
            % 64
    }

    pub(crate) fn add(&mut self, dr: i8, dg: i8, db: i8) {
        self.r = sum_with_wraparound(self.r, dr);
        self.g = sum_with_wraparound(self.g, dg);
        self.b = sum_with_wraparound(self.b, db);
    }

    pub fn rgb(r: u8, g: u8, b: u8) -> Self {
        Self { r, g, b, a: 255 }
    }

    pub fn rgb_float(r: f64, g: f64, b: f64) -> Self {
        Self {
            r: (r * 255.0).max(0.0).min(255.0) as u8,
            g: (g * 255.0).max(0.0).min(255.0) as u8,
            b: (b * 255.0).max(0.0).min(255.0) as u8,
            a: 255,
        }
    }

    pub fn rgba(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self { r, g, b, a }
    }
}

#[inline]
pub(crate) fn sum_with_wraparound(x: u8, y: i8) -> u8 {
    (x as i32 + y as i32) as u8
}
